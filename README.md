# lmde2-mate-priza, scripts licensed under GPLv3

customize lmde2 betsy MATE edition, for priza customers.
be careful! not to be used in general.

script to be run as regular user (not root!!!). eg:

$ wget https://framagit.org/stinpriza/lmde2-priza/raw/master/lmde2.sh

$ sh lmde2.sh

you'll be asked for sudo pass (needed for system updates, etc).
