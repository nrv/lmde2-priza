#!/bin/sh

wget https://framagit.org/stinpriza/lmde2-priza/raw/master/lmde2.sh
sh lmde2.sh

# calibri fonts
echo 'installing calibri fonts...'
mkdir ~/.fonts
wget https://framagit.org/stinpriza/lmde2-priza/raw/master/vistafonts-installer
chmod +x vistafonts-installer
sh ./vistafonts-installer
rm -f vistafonts-installer

#skype install
echo 'installing skype...'
sudo dpkg --add-architecture i386
sudo apt-get -qq update
sudo apt-get -y -qq install libc6:i386 libqt4-dbus:i386 libqt4-network:i386 libqt4-xml:i386 libqtcore4:i386 libqtgui4:i386 libqtwebkit4:i386 libstdc++6:i386 libx11-6:i386 libxext6:i386 libxss1:i386 libxv1:i386 libssl1.0.0:i386 libpulse0:i386 libasound2-plugins:i386
wget -O skype-install.deb http://www.skype.com/go/getskype-linux-deb
sudo dpkg -i skype-install.deb
sudo apt-get -qq -f install -y

# package cleanup
echo 'done, cleaning system...'
sudo apt-get purge -y -qq `deborphan`
sudo apt-get autoremove -y -qq
sudo apt-get clean -qq

echo 'all done, reboot now!'

exit 0
